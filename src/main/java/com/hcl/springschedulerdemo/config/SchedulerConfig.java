package com.hcl.springschedulerdemo.config;


import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class SchedulerConfig {
   
	@Scheduled(fixedDelay = 1000,initialDelay = 2000)
    public void scheduleFixedDelayTask() {
        System.out.println("Fixed delay task - " + System.currentTimeMillis() / 1000);
    }
	
	 
//	//runs at every 2 seconds
//	//It does not wait for the previous task to be complete. 
//	@Scheduled(fixedRate = 2000) public void scheduleTask()
//    {
// 
//        SimpleDateFormat dateFormat = new SimpleDateFormat(
//            "dd-MM-yyyy HH:mm:ss.SSS");
// 
//        String strDate = dateFormat.format(new Date());
// 
//        System.out.println(
//            "Fixed rate Scheduler: Task running at - "
//            + strDate);
//    }
	
	// Method
    // To trigger the scheduler every 3 seconds with
    // an initial delay of 5 seconds.
    @Scheduled(fixedDelay = 3000, initialDelay = 5000)
 
    public void scheduleTask()
    {
 
        SimpleDateFormat dateFormat = new SimpleDateFormat(
            "dd-MM-yyyy HH:mm:ss.SSS");
 
        String strDate = dateFormat.format(new Date());
 
        System.out.println(
            "Fixed Delay Scheduler: Task running at - "
            + strDate);
    }
}

